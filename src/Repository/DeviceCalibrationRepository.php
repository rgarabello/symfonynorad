<?php

namespace App\Repository;

use App\Entity\DeviceCalibration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DeviceCalibration|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeviceCalibration|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeviceCalibration[]    findAll()
 * @method DeviceCalibration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceCalibrationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeviceCalibration::class);
    }

    // /**
    //  * @return DeviceCalibration[] Returns an array of DeviceCalibration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeviceCalibration
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
