<?php

namespace App\Repository;

use App\Entity\Inspector;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Inspector|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inspector|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inspector[]    findAll()
 * @method Inspector[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InspectorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inspector::class);
    }

    // /**
    //  * @return Inspector[] Returns an array of Inspector objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Inspector
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
