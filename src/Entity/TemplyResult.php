<?php

namespace App\Entity;

use App\Repository\TemplyResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TemplyResultRepository::class)
 * @ORM\Table(name="temply_results")
 */
class TemplyResult
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Test::class, inversedBy="templyResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $test;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_time;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $alpha;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $radon;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $temperature;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $humidity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pressure;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $error;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $tilt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $power_outage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->date_time;
    }

    public function setDateTime(\DateTimeInterface $date_time): self
    {
        $this->date_time = $date_time;

        return $this;
    }

    public function getAlpha(): ?float
    {
        return $this->alpha;
    }

    public function setAlpha(?float $alpha): self
    {
        $this->alpha = $alpha;

        return $this;
    }

    public function getRadon(): ?float
    {
        return $this->radon;
    }

    public function setRadon(?float $radon): self
    {
        $this->radon = $radon;

        return $this;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function setTemperature(?float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getHumidity(): ?float
    {
        return $this->humidity;
    }

    public function setHumidity(?float $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getPressure(): ?float
    {
        return $this->pressure;
    }

    public function setPressure(?float $pressure): self
    {
        $this->pressure = $pressure;

        return $this;
    }

    public function getError(): ?bool
    {
        return $this->error;
    }

    public function setError(?bool $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function getTilt(): ?bool
    {
        return $this->tilt;
    }

    public function setTilt(?bool $tilt): self
    {
        $this->tilt = $tilt;

        return $this;
    }

    public function getPowerOutage(): ?bool
    {
        return $this->power_outage;
    }

    public function setPowerOutage(?bool $power_outage): self
    {
        $this->power_outage = $power_outage;

        return $this;
    }
}
