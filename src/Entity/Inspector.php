<?php

namespace App\Entity;

use App\Repository\InspectorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InspectorRepository::class)
 * @ORM\Table(name="inspectors")
 */
class Inspector
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="inspectors")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="inspectors")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $certification_number;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=DeviceInspector::class, mappedBy="inspector")
     */
    private $deviceInspectors;

    public function __construct()
    {
        $this->deviceInspectors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCertificationNumber(): ?string
    {
        return $this->certification_number;
    }

    public function setCertificationNumber(?string $certification_number): self
    {
        $this->certification_number = $certification_number;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|DeviceInspector[]
     */
    public function getDeviceInspectors(): Collection
    {
        return $this->deviceInspectors;
    }

    public function addDeviceInspector(DeviceInspector $deviceInspector): self
    {
        if (!$this->deviceInspectors->contains($deviceInspector)) {
            $this->deviceInspectors[] = $deviceInspector;
            $deviceInspector->setInspector($this);
        }

        return $this;
    }

    public function removeDeviceInspector(DeviceInspector $deviceInspector): self
    {
        if ($this->deviceInspectors->removeElement($deviceInspector)) {
            // set the owning side to null (unless already changed)
            if ($deviceInspector->getInspector() === $this) {
                $deviceInspector->setInspector(null);
            }
        }

        return $this;
    }
}
