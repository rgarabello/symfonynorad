<?php

namespace App\Entity;

use App\Repository\PassForgottenRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PassForgottenRequestRepository::class)
 * @ORM\Table(name="pass_forgotten_request")
 */
class PassForgottenRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="passForgottenRequests")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $passkey;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(?\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getPasskey(): ?string
    {
        return $this->passkey;
    }

    public function setPasskey(?string $passkey): self
    {
        $this->passkey = $passkey;

        return $this;
    }
}
