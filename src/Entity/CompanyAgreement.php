<?php

namespace App\Entity;

use App\Repository\CompanyAgreementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyAgreementRepository::class)
 * @ORM\Table(name="companies_agreements")
 */
class CompanyAgreement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="companyAgreements")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity=Agreement::class, inversedBy="companyAgreements")
     */
    private $agreement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $accepted;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getAgreement(): ?Agreement
    {
        return $this->agreement;
    }

    public function setAgreement(?Agreement $agreement): self
    {
        $this->agreement = $agreement;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAccepted(): ?\DateTimeInterface
    {
        return $this->accepted;
    }

    public function setAccepted(?\DateTimeInterface $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }
}
