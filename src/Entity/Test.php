<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 * @ORM\Table(name="tests")
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="tests")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=DeviceInspector::class, inversedBy="tests")
     */
    private $deviceInspector;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $addres_2;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $foor_level;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $room;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comments_begin;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comments_end;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $report_date;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $image_1;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $image_2;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $image_3;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $image_description_1;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $image_description_2;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $image_description_3;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $auth_code;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $transaction_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $receipt_id;

    /**
     * @ORM\ManyToOne(targetEntity=Payment::class, inversedBy="tests")
     */
    private $payment;

    /**
     * @ORM\OneToMany(targetEntity=TemplyResult::class, mappedBy="test")
     */
    private $templyResults;

    public function __construct()
    {
        $this->templyResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getDeviceInspector(): ?DeviceInspector
    {
        return $this->deviceInspector;
    }

    public function setDeviceInspector(?DeviceInspector $deviceInspector): self
    {
        $this->deviceInspector = $deviceInspector;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddres2(): ?string
    {
        return $this->addres_2;
    }

    public function setAddres2(?string $addres_2): self
    {
        $this->addres_2 = $addres_2;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getFoorLevel(): ?string
    {
        return $this->foor_level;
    }

    public function setFoorLevel(string $foor_level): self
    {
        $this->foor_level = $foor_level;

        return $this;
    }

    public function getRoom(): ?string
    {
        return $this->room;
    }

    public function setRoom(string $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getCommentsBegin(): ?string
    {
        return $this->comments_begin;
    }

    public function setCommentsBegin(?string $comments_begin): self
    {
        $this->comments_begin = $comments_begin;

        return $this;
    }

    public function getCommentsEnd(): ?string
    {
        return $this->comments_end;
    }

    public function setCommentsEnd(?string $comments_end): self
    {
        $this->comments_end = $comments_end;

        return $this;
    }

    public function getReportDate(): ?\DateTimeInterface
    {
        return $this->report_date;
    }

    public function setReportDate(?\DateTimeInterface $report_date): self
    {
        $this->report_date = $report_date;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getImage1(): ?string
    {
        return $this->image_1;
    }

    public function setImage1(?string $image_1): self
    {
        $this->image_1 = $image_1;

        return $this;
    }

    public function getImage2(): ?string
    {
        return $this->image_2;
    }

    public function setImage2(?string $image_2): self
    {
        $this->image_2 = $image_2;

        return $this;
    }

    public function getImage3(): ?string
    {
        return $this->image_3;
    }

    public function setImage3(?string $image_3): self
    {
        $this->image_3 = $image_3;

        return $this;
    }

    public function getImageDescription1(): ?string
    {
        return $this->image_description_1;
    }

    public function setImageDescription1(?string $image_description_1): self
    {
        $this->image_description_1 = $image_description_1;

        return $this;
    }

    public function getImageDescription2(): ?string
    {
        return $this->image_description_2;
    }

    public function setImageDescription2(?string $image_description_2): self
    {
        $this->image_description_2 = $image_description_2;

        return $this;
    }

    public function getImageDescription3(): ?string
    {
        return $this->image_description_3;
    }

    public function setImageDescription3(?string $image_description_3): self
    {
        $this->image_description_3 = $image_description_3;

        return $this;
    }

    public function getAuthCode(): ?string
    {
        return $this->auth_code;
    }

    public function setAuthCode(?string $auth_code): self
    {
        $this->auth_code = $auth_code;

        return $this;
    }

    public function getTransactionId(): ?int
    {
        return $this->transaction_id;
    }

    public function setTransactionId(?int $transaction_id): self
    {
        $this->transaction_id = $transaction_id;

        return $this;
    }

    public function getReceiptId(): ?int
    {
        return $this->receipt_id;
    }

    public function setReceiptId(?int $receipt_id): self
    {
        $this->receipt_id = $receipt_id;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return Collection|TemplyResult[]
     */
    public function getTemplyResults(): Collection
    {
        return $this->templyResults;
    }

    public function addTemplyResult(TemplyResult $templyResult): self
    {
        if (!$this->templyResults->contains($templyResult)) {
            $this->templyResults[] = $templyResult;
            $templyResult->setTest($this);
        }

        return $this;
    }

    public function removeTemplyResult(TemplyResult $templyResult): self
    {
        if ($this->templyResults->removeElement($templyResult)) {
            // set the owning side to null (unless already changed)
            if ($templyResult->getTest() === $this) {
                $templyResult->setTest(null);
            }
        }

        return $this;
    }
}
